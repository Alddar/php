<?php
    require_once "../autoload.php";

    $customer1 = (new Customer())
        ->setId(1)
        ->setName("Péťa");
    $customer2 = (new Customer())
        ->setId(2)
        ->setName("Jirka");

    $product1 = (new Product())
        ->setId(1)
        ->setName("Rohlík")
        ->setPrice(300);

    $product2 = (new Product())
        ->setId(2)
        ->setName("Máslo")
        ->setPrice(3500);

    $order1 = (new Order())
        ->setId(1)
        ->setCreated(new DateTime())
        ->setOrdered(new DateTime())
        ->setCustomer($customer1)
        ->addItem($product1);

    $order2 = (new Order())
        ->setId(2)
        ->setCreated(new DateTime())
        ->setOrdered(new DateTime())
        ->setCustomer($customer2)
        ->addItem($product1)
        ->addItem($product2);

    print_r($order1);
    print_r($order2);