<?php

class Order
{
    /** @var int|null */
    protected $id;

    /** @var \DateTime */
    protected $created;

    /** @var \DateTime */
    protected $ordered;

    /** @var Customer */
    protected $customer;

    /** @var Product[] */
    protected $items = [];

    function getId(): ?int {
        return $this->id;
    }

    function setId(?int $id): self {
        $this->id = $id;
        return $this;
    }

    public function getCreated(): ?\DateTime
    {
        return $this->created;
    }

    public function setCreated(?\DateTime $created): self
    {
        $this->created = $created;
        return $this;
    }

    public function getOrdered(): ?\DateTime
    {
        return $this->ordered;
    }

    public function setOrdered(?\DateTime $ordered): self
    {
        $this->ordered = $ordered;
        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;
        return $this;
    }

    public function getItems(): array
    {
        return $this->items;
    }

    public function setItems(array $items): self
    {
        $this->items = $items;
        return $this;
    }

    public function addItem(Product $product): self {
        $this->items[] = $product;
        return $this;
    }

    public function removeItem(Product $product): self {
        foreach($this->items as $key => $item) {
            if($item === $product) {
                unset($this->items[$key]);
            }
        }
        return $this;
    }
}