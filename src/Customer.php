<?php

class Customer
{
    /** @var int|null */
    protected $id;

    /** @var string|null */
    protected $name;

    function getId(): ?int {
        return $this->id;
    }

    function setId(?int $id): self {
        $this->id = $id;
        return $this;
    }

    function getName(): ?string {
        return $this->name;
    }

    function setName(?string $name): self {
        $this->name = $name;
        return $this;
    }
}